<?php

use Illuminate\Http\Request;

Route::post('login', 'Api\UserController@login');
Route::post('register', 'Api\UserController@register');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/subscribe', 'Api\UserController@subscribe')->middleware('auth:api');

Route::get('/games', 'Api\GameController@index');

Route::get('/anashed', 'Api\AnashedController@index');
Route::get('/anashed/{anashed}', 'Api\AnashedController@show');

Route::get('/stories', 'Api\StoriesController@index');
Route::get('/stories/{stories}', 'Api\StoriesController@show');

Route::get('/vegetable', 'Api\VegetableController@index');
Route::get('/vegetable/{vegetable}', 'Api\VegetableController@show');

Route::get('/safoora', 'Api\SafooraController@index');
Route::get('/safoora/{safoora}', 'Api\SafooraController@show');

Route::get('/mosba7', 'Api\Mosba7Controller@index');
Route::get('/mosba7/{mosba7}', 'Api\Mosba7Controller@show');

Route::get('/magazine/free', 'Api\MagazineController@free');

Route::middleware(['auth:api', 'subscribed:api'])->group(function () {

    Route::get('/magazine', 'Api\MagazineController@index');
    Route::get('/magazine/{magazine}', 'Api\MagazineController@show');

});