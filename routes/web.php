<?php

Route::get('/', function () {
    return redirect()->route('dashboard.welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
