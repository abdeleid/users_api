<?php

//welcome routes
Route::get('/', 'WelcomeController@index')->name('welcome');

//users routes
Route::get('/users/data', 'UserController@data')->name('users.data');
Route::resource('users', 'UserController')->only(['index', 'show', 'edit', 'update', 'destroy']);
