<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anashed extends Model
{
    protected $table = 'anashed';

    public function getRouteKeyName()
    {
        return 'slug';

    }//end of get route key name

}//end of model
