<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Magazine extends Model
{
    protected $table = 'magazine';

    public function getRouteKeyName()
    {
        return 'slug';

    }//end of gte route key name

}//end of model
