<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Safoora extends Model
{
    protected $table = 'safoora';

    public function getRouteKeyName()
    {
        return 'slug';

    }//end of get route key name

}//end of model
