<?php

namespace App\Http\Middleware;

use App\Traits\ApiResponse;
use Closure;

class Subscribed
{
    use ApiResponse;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard)
    {
        $subscribed = auth()->user()->subscribed;
        $image= auth()->user()->image;

        if ($guard == 'api') {

            if ($subscribed) {
                
                return $next($request);
                
            } else {
                
                if ($image) {
                    return $this->api_response([], 0, 'طلبك قيد المراجعه ستم تفغيل الحساب في اقرب وقت');
                }

                return $this->api_response([], 0, 'Unsubscribed');
                
            }//end of else not subscribed

        } else {

        }//end of else

    }//end of handle

}//end of subscribed
