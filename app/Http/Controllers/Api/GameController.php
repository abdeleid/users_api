<?php

namespace App\Http\Controllers\Api;

use App\Game;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GameController extends Controller
{
    use ApiResponse;

    public function index()
    {
        $games = Game::all();
        return $this->api_response($games);

    }//end of index

}//end of controller
