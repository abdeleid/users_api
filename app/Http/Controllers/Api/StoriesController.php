<?php

namespace App\Http\Controllers\Api;

use App\Stories;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StoriesController extends Controller
{
    use ApiResponse;

    public function index()
    {
        $stories = Stories::all();
        return $this->api_response($stories);

    }//end of index

    public function show(Stories $stories)
    {
        return $this->api_response($stories);

    }//end of show

}//end of controller
