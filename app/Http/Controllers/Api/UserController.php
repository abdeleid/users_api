<?php

namespace App\Http\Controllers\Api;

use App\Traits\ApiResponse;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class UserController extends Controller
{
    use ApiResponse;

    public $successStatus = 200;

    //login method
    public function login()
    {
        $user = User::where('email', request('email'))->first();

        if ($user) {

            $stretch_cost = 10;
            $salt = $user->salt;
            $hash = $user->hash;

            $login_password = crypt(request('password'), '$2a$' . $stretch_cost . '$' . $salt . '$');

            if ($hash == $login_password) {

                $data['token'] = $user->createToken('MyApp')->accessToken;
                $data['subscribed'] = $user->subscribed ? true : false;

                $message = '';

                if (!$user->subscribed && $user->images()->count() > 0) {
                    $message = 'طلبك قيد المراجعه ستم تفغيل الحساب في اقرب وقت';
                }

                $data['message'] = $message;
                return $this->api_response($data);

            } else {

                return $this->api_response([], 1, 'Unauthorized');

            }//end of else

        } else {

            return $this->api_response([], 1, 'Unauthorized');

        }//end of else

    }//end of login

    //register method
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->api_response([], 1, $validator->errors()->first());
        }

        $stretch_cost = 10;
        $salt = $this->_create_salt();

        $request_data = [
            'username' => $request->username,
            'email' => $request->email,
            'salt' => $salt,
            'hash' => null,
        ];

        $request_data['hash'] = crypt(request('password'), '$2a$' . $stretch_cost . '$' . $salt . '$');

        $user = User::create($request_data);

        $data['token'] = $user->createToken('MyApp')->accessToken;
        $data['username'] = $user->username;
        $data['subscribed'] = $user->subscribed ? true : false;

        return $this->api_response($data, 0, '');

    }//end of register

    protected function _create_salt()
    {
        $salt = $this->_pseudo_rand(128);
        return substr(preg_replace('/[^A-Za-z0-9_]/is', '.', base64_encode($salt)), 0, 21);
    }

    protected function _pseudo_rand($length)
    {
        if (function_exists('openssl_random_pseudo_bytes')) {
            $is_strong = false;
            $rand = openssl_random_pseudo_bytes($length, $is_strong);
            if ($is_strong === true)
                return $rand;
        }
        $rand = '';
        $sha = '';
        for ($i = 0; $i < $length; $i++) {
            $sha = hash('sha256', $sha . mt_rand());
            $chr = mt_rand(0, 62);
            $rand .= chr(hexdec($sha[$chr] . $sha[$chr + 1]));
        }
        return $rand;
    }

    public function subscribe(Request $request)
    {
        $request->validate([
            'image' => 'required',
        ]);

        if ($request->image) {
            $request->file('image')->store('public/images');

            auth()->user()->images()->create([
                'name' => $request->image->hashName()
            ]);

            // auth()->user()->update([
            //     'image' => $request->image->hashName()
            // ]);
        }
        $data['message'] = "تم استلام صوره التحويل البنكى و سيتم تفعيل الحساب في اقرب وقت ممكن.";
        return $this->api_response($data, 0, '');

    }// end of subscribe

}//end of controller
