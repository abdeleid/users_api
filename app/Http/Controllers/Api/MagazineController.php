<?php

namespace App\Http\Controllers\Api;

use App\Magazine;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MagazineController extends Controller
{
    use ApiResponse;

    public function index()
    {
        $magazine = Magazine::all();
        return $this->api_response($magazine);

    }//end of index

    public function show(Magazine $magazine)
    {
        return $this->api_response($magazine);

    }//end of show

    public function free()
    {
        $magazine = Magazine::where('free', 1)->get();
        return $this->api_response($magazine);

    }// end of free
    
}//end of controller
