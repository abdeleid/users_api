<?php

namespace App\Http\Controllers\Api;

use App\Anashed;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AnashedController extends Controller
{
    use ApiResponse;

    public function index()
    {
        $anashed = Anashed::all();
        return $this->api_response($anashed);

    }//end of index

    public function show(Anashed $anashed)
    {
        return $this->api_response($anashed);

    }//end of show

}//end of controller
