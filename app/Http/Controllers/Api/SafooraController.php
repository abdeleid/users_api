<?php

namespace App\Http\Controllers\Api;

use App\Safoora;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SafooraController extends Controller
{
    use ApiResponse;

    public function index()
    {
        $safoora = Safoora::all();
        return $this->api_response($safoora);

    }//end of index

    public function show(Safoora $safoora)
    {
        return $this->api_response($safoora);

    }//end of show

}//end of controller
