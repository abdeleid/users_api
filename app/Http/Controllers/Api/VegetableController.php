<?php

namespace App\Http\Controllers\Api;

use App\Traits\ApiResponse;
use App\Vegetable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VegetableController extends Controller
{
    use ApiResponse;

    public function index()
    {
        $vegetable = Vegetable::all();
        return $this->api_response($vegetable);

    }//end of index

    public function show(Vegetable $vegetable)
    {
        return $this->api_response($vegetable);

    }//end of show

}//end of controller
