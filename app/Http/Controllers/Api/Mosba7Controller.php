<?php

namespace App\Http\Controllers\Api;

use App\Mosba7;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Mosba7Controller extends Controller
{
    use ApiResponse;

    public function index()
    {
        $mosba7 = Mosba7::all();
        return $this->api_response($mosba7);

    }//end of index

    public function show(Mosba7 $mosba7)
    {
        return $this->api_response($mosba7);

    }//end of show

}//end of controller
