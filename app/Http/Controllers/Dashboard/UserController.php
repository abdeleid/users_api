<?php

namespace App\Http\Controllers\Dashboard;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    public function index()
    {
        return view('dashboard.users.index');

    }//end of index

    public function data()
    {
        $users = User::where('username', '!=', 'super_admin')->get();

        return DataTables::of($users)
            ->addColumn('subscribed', function($user) {
                if ($user->subscribed) {
                    return '<h4><span class="badge badge-success">' . __('site.subscribed') . '</span></h4>';
                }

                return '<h4><span class="badge badge-danger">' . __('site.not_subscribed') . '</span></h4>';
            })
            ->addColumn('action', 'dashboard.users.data_table.action')
            ->rawColumns(['subscribed', 'image', 'action'])
            ->toJson();

    }// end of data

    public function show(User $user)
    {
        return view('dashboard.users.show', compact('user'));

    }// end of show

    public function edit(User $user)
    {
        return view('dashboard.users.edit', compact('user'));

    }//end of edit

    public function update(Request $request, User $user)
    {
        $request->validate([
            'username' => 'required',
            'email' => 'required|unique:users,email,' . $user->id,
            'subscribed' => 'required',
        ]);
        
        $user->update($request->all());
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.users.index');
        
    }//end of update

    public function destroy(User $user)
    {
        $user->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.users.index');

    }//end of destroy
    
}//end of controller
