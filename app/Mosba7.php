<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mosba7 extends Model
{
    protected $table = 'mosba7';

    public function getRouteKeyName()
    {
        return 'slug';

    }//end of gte route key name

}//end of model
