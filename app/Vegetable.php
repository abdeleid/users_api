<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vegetable extends Model
{
    protected $table = 'vegetable';

    public function getRouteKeyName()
    {
        return 'slug';

    }//end of get route key name

}//end of model
