<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stories extends Model
{
    protected $table = 'stories';

    public function getRouteKeyName()
    {
        return 'slug';

    }//end of slug

}//end of stories
