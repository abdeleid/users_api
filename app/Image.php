<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    protected $fillable = ['name'];
    protected $appends = ['image_path'];

    public function getImagePathAttribute()
    {
        return Storage::url('images/' . $this->name);

    }// end of getImagePathAttribute

    public function imageable()
    {
        return $this->morphTo();

    }// end of imageable

}//end of model
