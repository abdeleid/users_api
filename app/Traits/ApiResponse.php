<?php

namespace App\Traits;

trait ApiResponse {

    public function api_response($data = [], $error_code = 0, $error_description = '')
    {
        return response()->json([
            'data' => $data,
           'errorCode' => $error_code,
           'errorDescription' => $error_description,
        ]);

    }//end of api response

}//end of api response