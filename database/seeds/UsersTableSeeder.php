<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $stretch_cost = 10;
        $salt = $this->_create_salt();

        $user = User::create([
            'username' => 'super_admin',
            'email' => 'super_admin@app.com',
            'salt' => $salt,
            'hash' => crypt('password', '$2a$' . $stretch_cost . '$' . $salt . '$'),
            'password' => bcrypt('password'),
            'subscribed' => true,
        ]);

        $user->attachRole('super_admin');

    }//end of run

    protected function _create_salt()
    {
        $salt = $this->_pseudo_rand(128);
        return substr(preg_replace('/[^A-Za-z0-9_]/is', '.', base64_encode($salt)), 0, 21);
    }

    protected function _pseudo_rand($length)
    {
        if (function_exists('openssl_random_pseudo_bytes')) {
            $is_strong = false;
            $rand = openssl_random_pseudo_bytes($length, $is_strong);
            if ($is_strong === true)
                return $rand;
        }
        $rand = '';
        $sha = '';
        for ($i = 0; $i < $length; $i++) {
            $sha = hash('sha256', $sha . mt_rand());
            $chr = mt_rand(0, 62);
            $rand .= chr(hexdec($sha[$chr] . $sha[$chr + 1]));
        }
        return $rand;
    }
}//end of seeder
