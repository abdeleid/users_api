<?php

return [
    'welcome' => 'الرئسيه',
    'logout' => 'تسجيل الخروج',
    'login' => 'تسجيل الدخول',

    'add' => 'اضف',
    'edit' => 'تعديل',
    'delete' => 'حذف',
    'action' => 'اكشن',
    'search' => 'بحث',
    'no_data_found' => 'للاسف لايوحد اي سجلات',
    'added_successfully' => 'تم اضافه السجلات بنجاح',
    'updated_successfully' => 'تم تعديل السجلات بنجاح',
    'deleted_successfully' => 'تم حذف السجلات بنجاح',
    'confirm_delete' => 'تاكيد الحذف',
    'yes' => 'نعم',
    'no' => 'لا',
    'hi' => 'اهلا',
    'choose' => 'اختر',
    'create' => 'اضافه',
    'read' => 'عرض',
    'update' => 'تعديل',
    'delete' => 'حذف',
    'show' => 'عرض',

    'roles' => 'الوظايف',
    'permissions' => 'الصلاحيات',
    'name' => 'الاسم',
    'users_count' => 'عدد المستخدمين',

    'users' => 'المستخدمين',
    'user_name' => 'اسم المستخدم',
    'email' => 'البريد الاكتروني',
    'subscribed' => 'مشترك',
    'not_subscribed' => 'ليس مشترك',
    'subscription_start_date' => 'تاريخ بدا الاشتراك',
    'subscription_end_date' => 'تاريخ انتهاء الاشتراك',
    'images' => 'الصور',
    'image' => 'الصوره',

    'settings' => 'الاعدادات',
    'social_login' => 'الدخول بواسطه وسائل التواصل',
    'social_links' => 'روابط وسائل التواصل',

];