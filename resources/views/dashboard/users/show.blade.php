@extends('layouts.dashboard.app')

@section('content')
    <div>
        <h2>@lang('site.users')</h2>
    </div>

    <ul class="breadcrumb mt-2">
        <li class="breadcrumb-item"><a href="{{ route('dashboard.welcome') }}">@lang('site.welcome')</a></li>
        <li class="breadcrumb-item"><a href="{{ route('dashboard.users.index') }}">@lang('site.users')</a></li>
        <li class="breadcrumb-item">@lang('site.show')</li>
    </ul>

    <div class="row">

        <div class="col-md-12">

            <div class="tile shadow">

                <div class="form-group">
                    <label>@lang('site.user_name')</label>
                    <p>{{ $user->username }}</p>
                </div>

                <div class="form-group">
                    <label>@lang('site.email')</label>
                    <p>{{ $user->email }}</p>
                </div>

                <div class="form-group">
                    <label>@lang('site.subscribed')</label>
                    @if ($user->subscribed)

                        <p>@lang('site.yes')</p>
                    @else
                        <p>@lang('site.no')</p>
                    @endif
                </div>

                @if ($user->subscribed)

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>@lang('site.subscription_start_date')</label>
                                <p>{{ $user->subscription_start_date }}</p>
                            </div>
                        </div><!-- end of col -->

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>@lang('site.subscription_end_date')</label>
                                <p>{{ $user->subscription_end_date }}</p>
                            </div>
                        </div>

                    </div><!-- end of row -->

                @endif

                <div class="form-group">
                    <label>@lang('site.images')</label>
                    @forelse ($user->images as $image)
                        <p>
                            <img src="{{ $image->image_path }}" style="width: 300px;" alt="">
                        </p>
                    @empty
                        <p>@lang('site.no_data_found')</p>
                    @endforelse
                </div>

            </div><!-- end of tile -->

        </div><!-- end of col -->

    </div><!-- end of row -->
@endsection