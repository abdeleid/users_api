@extends('layouts.dashboard.app')

@section('content')

    <div>
        <h2>@lang('site.users')</h2>
    </div>

    <ul class="breadcrumb mt-2">
        <li class="breadcrumb-item"><a href="{{ route('dashboard.welcome') }}">@lang('site.welcome')</a></li>
        <li class="breadcrumb-item"><a href="{{ route('dashboard.users.index') }}">@lang('site.users')</a></li>
        <li class="breadcrumb-item">@lang('site.edit')</li>
    </ul>

    <div class="row">

        <div class="col-md-12">

            <div class="tile shadow">

                <form method="post" action="{{ route('dashboard.users.update', $user->id) }}">
                    @csrf
                    @method('put')

                    {{--username--}}
                    <div class="form-group">
                        <label>@lang('site.user_name')</label>
                        <input type="text" name="username" class="form-control" value="{{ old('username', $user->username) }}">
                    </div>
                    
                    {{--email--}}
                    <div class="form-group">
                        <label>@lang('site.email')</label>
                        <input type="text" name="email" class="form-control" value="{{ old('email', $user->email) }}">
                    </div>

                    <div class="form-group">
                        <label>@lang('site.subscribed')</label>
                        <select name="subscribed" class="form-control">
                            <option value="1" {{ $user->subscribed == 1 ? 'selected' : '' }}>@lang('site.subscribed')</option>
                            <option value="0" {{ $user->subscribed == 0 ? 'selected' : '' }}>@lang('site.not_subscribed')</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> @lang('site.edit')</button>
                    </div>

                </form><!-- end of form -->

            </div><!-- end of tile -->

        </div><!-- end of col -->

    </div><!-- end of row -->

@endsection