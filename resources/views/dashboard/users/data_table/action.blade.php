<a href="{{ route('dashboard.users.show', $id) }}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> @lang('site.show')</a>
<a href="{{ route('dashboard.users.edit', $id) }}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i> @lang('site.edit')</a>
<form action="{{ route('dashboard.users.destroy', $id) }}" style="display: inline-block;" method="post">
    @csrf
    @method('delete')
    <button class="btn btn-danger btn-sm delete"><i class="fa fa-trash"></i> @lang('site.delete')</button>
</form>