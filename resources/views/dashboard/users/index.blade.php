@extends('layouts.dashboard.app')

@section('content')

    <div>
        <h2>@lang('site.users')</h2>
    </div>

    <ul class="breadcrumb mt-2">
        <li class="breadcrumb-item"><a href="{{ route('dashboard.welcome') }}">@lang('site.welcome')</a></li>
        <li class="breadcrumb-item">@lang('site.users')</li>
    </ul>

    <div class="row">

        <div class="col-md-12">

            <div class="tile shadow">

                <table class="table table-hover" id="users-table">
                    <thead>
                    <tr>
                        <th>@lang('site.user_name')</th>
                        <th>@lang('site.email')</th>
                        <th>@lang('site.subscribed')</th>
                        <th>@lang('site.action')</th>
                    </tr>
                    </thead>
                </table>
            </div><!-- end of tile -->

        </div><!-- end of col -->

    </div><!-- end of row -->
@endsection

@push('scripts')

    <script>
        $('#users-table').DataTable({
            serverSide: true,
            processing: true,
            ajax: {
                url: '{!! route('dashboard.users.data') !!}',
            },
            // order: [[1, "asc"]],
            columns: [
                {data: 'username', name: 'username'},
                {data: 'email', name: 'email'},
                {data: 'subscribed', name: 'subscribed'},
                {data: 'action', name: 'action', sortable: false, searchable: false},
            ],
        });
    </script>
@endpush